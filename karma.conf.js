module.exports = function(config){
    config.set({

        basePath : './',

        files : [
            'webroot/themes/beta/js/vendors.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'webroot/themes/beta/js/app.js',
            'test/**/*.js'
        ],

        autoWatch : true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        junitReporter : {
            outputFile: 'test_out/unit.xml',
            suite: 'test'
        }

    });
};
describe('Gallery Main Controller', function() {

    beforeEach(module('GalleryApp'));

    var scope, $location, createController;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();

        createController = function() {
            return $controller('MainController', {
                '$scope': scope
            });
        };
    }));

    it('Check the galley is set', function() {
        var controller = createController();
        expect(scope.gallery).toEqual(null);
    });

    it('Check the tag used in Flick is London', function() {
        var controller = createController();
        console.dir(controller);
        expect(controller.tags).toEqual("london");
    });

    it('Check selectedImages is an Array', function() {
        var controller = createController();
        console.dir(controller);
        expect(controller.selectedImages).toEqual([]);
    });

});
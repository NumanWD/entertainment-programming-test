module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig(
        {
            pkg: grunt.file.readJSON('package.json'),

            watch: {
                sass: {
                    files: ['<%= pkg.theme %>/sass/**/*.scss'],
                    tasks: ['sass:project', 'concat:css']
                }
            },

            sass: {
                project: {
                    options: {
                        style: 'expanded'
                    },
                    files: [{
                        expand: true,
                        cwd: '<%= pkg.theme %>/sass/',
                        src: ['*.scss'],
                        dest: '<%= pkg.theme %>/<%= pkg.cssDir %>/',
                        ext: '.css'
                    }]
                }
            },

            concat: {
                options: {
                    separator: '\n'
                },
                css: {
                    src: [
                        '<%= pkg.bowerDir %>/normalize-css/normalize.css',
                        '<%= pkg.theme %>/<%= pkg.cssDir %>/screen.css'
                    ],
                    dest: '<%= pkg.theme %>/<%= pkg.cssDir %>/main.css'
                },
                js_vendors: {
                    src: [
                        '<%= pkg.bowerDir %>/angular/angular.js',
                        '<%= pkg.bowerDir %>/angular-cookies/angular-cookies.js'

                    ],
                    dest: '<%= pkg.theme %>/js/vendors.js'
                },
                js: {
                    src: [
                        '<%= pkg.theme %>/js/ngApp/*.js'

                    ],
                    dest: '<%= pkg.theme %>/js/app.js'
                }

            },
            connect: {
                server: {
                    options: {
                        port: 8080,
                        base: 'webroot',
                        open: true
                    }
                }
            }
        }
    );

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');


    // Default task(s).

    grunt.registerTask('local', ['sass:project', 'concat', 'connect', 'watch']);

};
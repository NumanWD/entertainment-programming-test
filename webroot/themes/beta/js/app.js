(function () {
    /**
     * Start the GalleryApp
     */
    angular.module('GalleryApp', ['ngCookies'])
        .controller('MainController', [
            '$scope', '$http', '$cookies',
            MainController
        ]);

    /**
     * Main Controller of the app
     *
     * @param $scope
     * @param $http
     * @param $cookies
     */
    function MainController($scope, $http, $cookies) {

        var that = this;

        $scope.gallery = null;

        that.tags = 'london';
        that.src = 'http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=cb&tags=' + that.tags;
        that.selectedImages = [];


        /**
         * Callback from flick, return all the images
         * @param data
         */
        cb = function (data) {
            $scope.gallery = data.items;
        };

        /**
         * Toggle the field selected in the object image
         *
         * @param image
         */
        $scope.toggle = function (image) {

            var id = /([0-9]+)\/$/.exec(image.link);

            image.selected = !image.selected;

            if (image.selected) {
                selectedImages.push(id[1]);
            } else if (selectedImages.indexOf(id[1]) > -1){
                selectedImages.splice(selectedImages.indexOf(id[1]), 1);
            }
            $cookies.putObject('selection', selectedImages);
        };

        /**
         * Check in the cookies if the image has been selected
         *
         * @param image
         */
        $scope.isSelected = function (image) {

            var id = /([0-9]+)\/$/.exec(image.link);

            if (index = selectedImages.indexOf(id[1]) > -1){
                image.selected = true;
            }
        };


        /**
         * Init the app
         */
        that.init = function() {

            var cookies = $cookies.getObject('selection');
            $http.jsonp(that.src);

            if (typeof cookies != "undefined") {
                selectedImages = cookies;
            }
        }();
    }
})();

## Check the Exercise 
 
Access the Webroot and open the index.html in the browser.

I changed a bit the structure of the exercise putting the js in a file to allow me to run the test.


## Requirements Develop

- Ruby (via RVM, tested with version >= 2.2.1p85)
- Ruby Gem Sass
- Node (via NVM, tested with version >= 0.10.40)

## Set up Dev Env

The current project uses dependencies from both Node and Bower:

    $ npm install
   
## Working

You can simply run it from the root of the project Grunt local and It will execute several tasks as sass, concat css, watch for changes and create a server:

    $ grunt local

## Run the Test

About the test, I have just set up the env to test the main controller, and a couple of very basic assertions.

    $ npm test
    
    
## What browsers is work with
    
The browser support is based in Angular support.    
Following browsers: the latest versions of Chrome, Firefox, Safari, and Safari for iOs, as well as Internet Explorer versions 9-11.